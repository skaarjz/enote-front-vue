module.exports = {
  root: true,
  parserOptions:{
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
    node: true
  },
  extends: [
    "@nuxtjs"
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    "no-console": "off",
    "vue/require-prop-types": "off"
  },
  globals: {}
}

const nodeExternals = require('webpack-node-externals')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
require('dotenv').config()

console.log(process.env.API_URL)

module.exports = {
  server: {
    // nuxt.js server options ( can be overrided by environment variables )
    port: 8000,
    host: '0.0.0.0'
  },
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Енот - записная книжка онлайн',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js + Vuetify.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.8.1/css/all.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    '@nuxtjs/auth'
  ],
  styleResources: {
    scss: [
      'assets/scss/file/_path.scss',
      'assets/scss/file/_path-two.scss'
    ]
  },
  axios: {
    baseURL: process.env.NODE_ENV === 'development' ? process.env.DEV_API : process.env.PROD_API,
    credentials: false,
    proxy: false,
    debug: false
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/auth/login', method: 'post', propertyName: 'accessToken' },
          user: { url: '/auth/me', method: 'get', propertyName: false },
          logout: false
        }
      }
    },
    cookie: false,
    rewriteRedirects: true
  },
  router: {
    middleware: ['auth']
  },
  plugins: [
    '~/plugins/vuetify.js',
    '~/plugins/redirect401.js',
    '~/plugins/notify.js',
    '~/plugins/picker.js',
    '~/plugins/color-picker.js',
    '~/plugins/mixins.js'
  ],
  css: ['~/assets/style/app.styl'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#5CB85C' },
  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vuetify/],
    vendor: ['vue-the-mask'],
    plugins: [
      new VuetifyLoaderPlugin()
    ],
    babel: {
      plugins: ['@babel/plugin-proposal-optional-chaining']
    },
    extractCSS: true,
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (process.server) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}

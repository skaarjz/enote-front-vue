import Vue from 'vue'
import Vuetify, {
  VApp, // required
  VNavigationDrawer,
  VFooter,
  VToolbar
} from 'vuetify/lib'
import { Ripple } from 'vuetify/lib/directives'
import ru from 'vuetify/es5/locale/ru'

Vue.use(Vuetify, {
  lang: {
    locales: { ru },
    current: 'ru'
  },
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VToolbar
  },
  directives: {
    Ripple
  }
})

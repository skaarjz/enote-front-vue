import Vue from 'vue'

Vue.mixin({
  methods: {
    $formatDate(data) {
      if (!data) {
        return new Date().toISOString().substr(0, 10)
      } else {
        return new Date(data).toLocaleDateString('ru')
      }
    }
  }
})

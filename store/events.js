
const mutations = {
  SET_USERS_LIST(state, data) {
    state.usersList = data
  },
  SET_EVENTS(state, data) {
    state.events = data
  },
  SET_SUM_EVENTS(state, data) {
    state.sumEvents = data
  }
}

const actions = {
  async getUsersList({ commit }) {
    try {
      const data = await this.$axios.$get('/users/users-list')
      commit('SET_USERS_LIST', data)
    } catch (e) {
      console.error(e)
    }
  },
  async getEvents({ commit }) {
    const data = await this.$axios.$get('/events/events')
    commit('SET_EVENTS', data)
  },
  async getSumEventsForPeriod({ commit }, date) {
    const data = await this.$axios.$post('/events/get-events-for-period', date)
    if (data.length) {
      const sumEvents = data.map(event => event.price).reduce((a, b) => { return a + b })
      commit('SET_SUM_EVENTS', sumEvents)
    } else return null
  },
  async createEventRequest({ commit, dispatch }, requestData) {
    const services = await this.$axios.$post('/services/get-services-price', requestData.selected_services)
    if (services?.service[0]?.price) {
      requestData.price = services.service.map(service => service.price).reduce((accumulator, currentValue) => accumulator + currentValue)
    } else {
      requestData.price = 0
    }
    await this.$axios.post(`/events/add-event`, requestData)
  },
  async editEvent({ commit, dispatch }, { data, id }) {
    const services = await this.$axios.$post('/services/get-services-price', data.selected_services)
    if (services?.service[0]?.price) {
      data.price = services.service.map(service => service.price).reduce((accumulator, currentValue) => accumulator + currentValue)
    } else {
      data.price = 0
    }
    await this.$axios.put(`/events/edit-event?eventID=${id}`, data)
  },
  async deleteEvent({ commit, dispatch }, { id }) {
    try {
      await this.$axios.delete(`/events/delete-event?eventID=${id}`)
      dispatch('getEvents')
    } catch (e) {
      throw new Error(e)
    }
  }
}

export default {
  state() {
    return {
      events: [],
      sumEvents: 0,
      usersList: []
    }
  },
  actions,
  mutations
}

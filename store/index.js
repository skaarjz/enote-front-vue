import services from './services'
import spending from './spending'
import events from './events'

export default {
  modules: {
    services,
    spending,
    events
  }
}

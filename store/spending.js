
const mutations = {
  SET_SPENDING(state, data) {
    state.spending = data
  },
  SET_SUM_SPENDING(state, data) {
    state.sumSpending = data
  }
}

const actions = {
  // async getSpending({ commit }) {
  //   try {
  //     const currentDate = new Date()
  //     const datePosted = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)
  //     const data = {
  //       date_posted: datePosted
  //     }
  //     const responseData = await this.$axios.post('/spending/spending', data)
  //     commit('SET_SPENDING', responseData)
  //   } catch (e) {
  //     console.log(e)
  //   }
  // },

  async getSumSpendingForPeriod({ commit }, date) {
    const data = await this.$axios.$post('/spending/get-spending-for-period', date)
    commit('SET_SPENDING', data)
  },

  async createSpending({ commit, dispatch }, requestData) {
    await this.$axios.post(`/spending/add-spending`, requestData)
  },
  async deleteSpending({ commit, dispatch }, id) {
    await this.$axios.delete(`/spending/delete-spending?spendingID=${id}`)
  }
}

export default {
  state() {
    return {
      spending: [],
      sumSpending: 0
    }
  },
  actions,
  mutations
}

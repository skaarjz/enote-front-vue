
const mutations = {
  SET_CATEGORIES(state, data) {
    state.categories = data
  },
  SET_SERVICES(state, data) {
    state.services = data
  },
  SET_SERVICE(state, data) {
    state.service = data
  },
  UPDATE_SERVICE(state, data) {
    const { key, event } = data
    state.service[key] = event
  }
}

const getters = {
  filteredService(state) {
    return state.categories.reduce((accCategories, nextCategory) => {
      const services = state.services.reduce((accServices, nextService) => {
        if (nextCategory._id === nextService.category_id) {
          return [...accServices, nextService]
        } return accServices
      }, [])
      if (services.length) {
        return [...accCategories, { name: nextCategory.name, services }]
      } return accCategories
    }, [])
  }
}

const actions = {
  async getCategories({ commit }) {
    const data = await this.$axios.$get('/categories/categories')
    commit('SET_CATEGORIES', data)
  },

  async deleteCategory({ commit, dispatch }, categoryId) {
    try {
      const { id } = categoryId
      await this.$axios.delete(`/categories/delete-category?categoryID=${id}`)
      dispatch('getCategories')
    } catch (e) {
      console.error(e)
    }
  },
  async createCategory({ commit, dispatch }, responseData) {
    try {
      if (!responseData.name) {
        throw new Error('Имя категории не может быть пустым')
      }
      await this.$axios.post(`/categories/add-category`, responseData)
      dispatch('getCategories')
    } catch (e) {
      console.error(e.message)
    }
  },
  async getServices({ commit }) {
    const data = await this.$axios.$get('/services/services')
    commit('SET_SERVICES', data)
  },
  async getService({ commit }, id) {
    const data = await this.$axios.$get(`/services/service/${id}`)
    commit('SET_SERVICE', data)
    return this.state.service
  },
  async createService({ commit, dispatch }, responseData) {
    await this.$axios.post(`/services/add-service`, responseData)
  },
  async deleteService({ commit, dispatch }, id) {
    await this.$axios.delete(`/services/delete-service?serviceID=${id}`)
  },
  async editService({ commit, dispatch }, { data, id }) {
    try {
      await this.$axios.$put(`/services/edit-service?serviceID=${id}`, data)
      dispatch('getServices')
    } catch (e) {
      console.log(e)
    }
  }
}

export default {
  state() {
    return {
      categories: [],
      services: [],
      service: [],
      eventInfo: {}
    }
  },
  actions,
  mutations,
  getters
}

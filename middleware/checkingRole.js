export default function ({ store, error }) {
  // If the user is not have access to page
  if (store.$auth.user.role !== 'admin') {
    return error({ statusCode: 404, message: 'Страница не существует' })
  }
}
